﻿using System;
using System.Activities;
using System.ComponentModel;
using Excel = Microsoft.Office.Interop.Excel;

namespace ExcelToPDFLibrary
{

    public class ExcelToPdf : CodeActivity
    {
        [Category("Input")]
        [Description("Full path include file's name with excel extension - c:/Users/chanon/input_file.xlsx")]
        [RequiredArgument]
        public InArgument<string> ExcelFilePath { get; set; }

        [Category("Input")]
        [Description("Destination sheet to convert")]
        [RequiredArgument]
        public InArgument<string> SheetName { get; set; }

        [Category("Input")]
        [Description("Full path include file's name with pdf extension - c:/Users/chanon/output_file.pdf")]
        [RequiredArgument]
        public InArgument<string> PDFOutputFilePath { get; set; }

        [Category("Input")]
        [Description("Choose paper size")]
        [DefaultValue(Excel.XlPaperSize.xlPaperA4)]
        [RequiredArgument]
        public Excel.XlPaperSize PaperSize { get; set; }

        [Category("Output")]
        public OutArgument<string> RuntimeErrorMessage { get; set; }

        protected static void ExcelToPDF(string excelFilePath, string pdfOutputFilePath, string sheetName, Excel.XlPaperSize paperSize)
        {
            LogHelper logHelper = new LogHelper();
            try
            {
                Console.WriteLine(logHelper.BuildLog(LogHelper.logLevelEnum.Information, LogHelper.methodEnum.ExcelToPDF, " Start."));

                //Replace for 
                //ExcelToPDF("C:/Users/chano/Desktop/Book1.xlsx", "C:/Users/chano/Desktop/1/kg_sheet2.pdf", "Sheet2"); 
                //to ExcelToPDF("C:\\Users\\chano\\Desktop\\Book1.xlsx", "C:\\Users\\chano\\Desktop\\1\\kg_sheet5.pdf", "Sheet2"); for easy to use
                excelFilePath = excelFilePath.Replace("/", "\\");
                pdfOutputFilePath = pdfOutputFilePath.Replace("/", "\\");

                Excel.Application excelApplication = new Excel.Application();
                excelApplication.Visible = false;
                excelApplication.DisplayAlerts = false;
                Excel.Workbook wkb = excelApplication.Workbooks.Open(excelFilePath, ReadOnly: true);

                Excel.Worksheet sheet = (Excel.Worksheet)wkb.Sheets[sheetName];

                ((Excel._Worksheet)wkb.ActiveSheet).PageSetup.PaperSize = paperSize;//Excel.XlPaperSize.xlPaperA4; //Custom paper size
                //((Microsoft.Office.Interop.Excel._Worksheet) wkb.ActiveSheet).PageSetup.Orientation = Microsoft.Office.Interop.Excel.XlPageOrientation.xlLandscape; //if need to be landscape
                if (wkb != null)
                {
                    sheet.ExportAsFixedFormat(
                                            Microsoft.Office.Interop.Excel.XlFixedFormatType.xlTypePDF,
                                            @pdfOutputFilePath,
                                            Microsoft.Office.Interop.Excel.XlFixedFormatQuality.xlQualityStandard);
                }
                else
                {
                    Console.WriteLine(logHelper.BuildLog(LogHelper.logLevelEnum.Warning, LogHelper.methodEnum.ExcelToPDF, " Workbook null."));
                }

                wkb.Close();
                excelApplication.Quit();
                GC.Collect();
            }
            catch (Exception e)
            {
                Console.WriteLine(logHelper.BuildLog(LogHelper.logLevelEnum.Error, LogHelper.methodEnum.ExcelToPDF, e.Message + " Stacktrace: " + e.StackTrace));
            }
            finally
            {
                Console.WriteLine(logHelper.BuildLog(LogHelper.logLevelEnum.Information,LogHelper.methodEnum.ExcelToPDF," End."));
            }
        }

        protected override void Execute(CodeActivityContext context)
        {
            LogHelper logHelper = new LogHelper();
            try
            {
                Console.WriteLine(logHelper.BuildLog(LogHelper.logLevelEnum.Information,LogHelper.methodEnum.Execute," Start."));

                if (!string.IsNullOrEmpty(ExcelFilePath.Get(context)) 
                    && !string.IsNullOrEmpty(PDFOutputFilePath.Get(context)) 
                    && !string.IsNullOrEmpty(SheetName.Get(context)))
                {
                    ExcelToPDF(ExcelFilePath.Get(context), PDFOutputFilePath.Get(context), SheetName.Get(context), PaperSize);
                }
                else
                {
                    Console.WriteLine(logHelper.BuildLog(LogHelper.logLevelEnum.Warning, LogHelper.methodEnum.Execute
                        , " ExcelFilePath's Value: ["+ ExcelFilePath.Get(context) 
                        + "] PDFOutputPath's Value: [" + PDFOutputFilePath.Get(context)
                        + "] SheetName's Value: [" + SheetName.Get(context)
                        + "] is Null or Empty."));
                }
            }
            catch (Exception e)
            {
                RuntimeErrorMessage.Set(context, logHelper.BuildLog(LogHelper.logLevelEnum.Error,LogHelper.methodEnum.Execute, e.Message+" Stacktrace: "+e.StackTrace));
            }
            finally
            {
                Console.WriteLine(logHelper.BuildLog(LogHelper.logLevelEnum.Information, LogHelper.methodEnum.Execute, " End."));
            }

        }
    }


}
