﻿using System;

namespace ExcelToPDFLibrary
{
    class LogHelper
    {
        //ExcelToPDFLibrary: Level:[Warning] Method:Execute Message:ExcelFilePath is Null or Empty
        private string activitiesName = "ExcelToPDFLibrary: ";

        public enum logLevelEnum
        {
            Information = 0,
            Warning = 1,
            Error = 2
        }
        private string[] logLevel = Enum.GetNames(typeof(logLevelEnum));

        public enum methodEnum
        {
            Execute = 0,
            ExcelToPDF = 1
        }
        private string[] methodName = Enum.GetNames(typeof(methodEnum));

        public string BuildLog(logLevelEnum _logLevel, methodEnum _method, string _message)
        {
            return activitiesName
                    + "Level:[" + logLevel[Convert.ToInt32(_logLevel)]
                    + "] Method:" + methodName[Convert.ToInt32(_method)]
                    + " Message: "+_message;
        }
    }
}
